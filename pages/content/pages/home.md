Title: Unterricht und Knowhow mit offenen Karten
Date: 2018-07-11
Category: OpenSchoolMaps
Tags: meta, mission statement
Status: hidden
Url:
Save_as: index.html

![]({filename}/images/schoolkids.jpg)

OpenSchoolMaps ist ein Projekt zur Förderung offener Karten im Unterricht, 
sei es in digitaler Form, im Browser, auf mobilen Geräten oder gedruckt. 
Dabei werden vornehmlich freie Daten von OpenStreetMap eingesetzt.

Angesprochen sind Lehrpersonen auf Sekundarstufe II (Sekundarschulen und Gymnasien) sowie weitere Interessierte.

Sowohl die Karten und Karten-Daten als auch die Lehr- und Lernmaterialien werden 
als Open Educational Resource ([OER](https://de.wikipedia.org/wiki/Open_Educational_Resources)) 
bereitgestellt, so dass jeder zu den Lehr- und Lernmaterialien beitragen, 
sie frei - auch ausserhalb des Unterrichts! - nutzen und weitergeben kann und darf.

## News

- Workshop zu "OER in der Schule - OpenSchoolMaps und H5P" von Nicole Berva und Stefan Keller am Sa. 6. April 2019: "Open Education Day 2019" in Bern. <https://openeducationday.ch/>
- Vortrag von Stefan Keller zu "OpenSchoolMaps - Unterricht mit offenen Karten" am Sa. 16. März in Wil/SG an der ICT-Kadervernetzung 2019 vor Kursleitenden in Medien und Informatik aller Ostschweizer Kantone sowie Liechtenstein. <http://educanet2.ch/kadervernetzung/.ws_gen/>
- All worksheets (PDF) from OpenSchoolMaps - including the introductions to OpenStreetMap and to QGIS 3 - are available in English now. (2019-03-15) 
- New post on Geometa Lab HSR Blog published 2018-01-15, about ["Creating a Thematic Online-Map using uMap"](http://geometalab.tumblr.com/post/182036823612/creating-a-thematic-online-map-using-umap).
- Am Sa. 6. Oktober 2018 hielt Stefan Keller anlässlich der WikiCon 2018 in St. Gallen einen Vortrag mit dem Titel "OpenSchoolMaps mit OpenStreetMap und Geodaten editieren mit QGIS". <https://wikicon.org>
- Am 3. Juli 2018 wurde anlässlich der Opendata.ch/2018-Konferenz in St. Gallen ein "Community Showcase" vorgestellt mit dem Titel "OpenSchoolMaps - OpenStreetMap als Open Educational Resource". <https://hack.opendata.ch/project/204>
- Am 6. und 7. Juni 2018 gab es einen Edu Workshop für Lehrpersonen am GEOSchool Day in Bern. Der Beitrag von Stefan Keller hatte den Titel "OpenStreetMap". <http://geoschoolday.ch/> (vgl. Folien).
- Am 28. April 2018 wurde anlässlich des "Open Education Day 2018", FHNW Brugg, erstmals von Stefan Keller OpenSchoolMaps vorgestellt Hier <https://openeducationday.ch/das-programm/die-freie-weltkarte-openstreetmap-grenzueberschreitend-nutzen/> die Folien und hier der Workshop "Die freie Weltkarte OpenStreetMap grenzüberschreitend nutzen" <https://openeducationday.ch/>

Bildquelle: Thomas Ingold 2018